import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class TodoItem extends Component {
    // componentWillMount() {
    //     getStyle()
    // }

    getStyle = () => {

        return {
            background: '#f4f4f4',
            padding: '10px',
            borderBottom:'1px dotted #ccc',
            textDecoration: this.props.todo.completed
            ? 'line-through' 
            : 'none' 
        }   

        // if(this.props.todo.completed) {
        //     return {
        //         textDecoration: 'line-through'
        //     }
        // } else {
        //     return {
        //         textDecoration: 'none'
        //     }  
    }




    render() {
        const { id, title } = this.props.todo;
    return (
      <div style={this.getStyle()}>
        <p> 
            <input type='checkbox' onChange={()=>this.props.toggleComplete(id)}/> 
            {' '} 
        { title} 
        <button style={btnStyle} onClick={()=>this.props.delTodo(id)}>x</button>
        </p>

      </div>
    )
  }
}



//PropTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired
}

const btnStyle = {
    background: '#ff0000',
    color:'#ffff',
    padding: '4px 8px',
    border: 'none',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right'
}

export default TodoItem
